﻿using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

using System.IO;
using FirstFloor.ModernUI.Windows.Controls;
using System.Threading;


namespace WOK_SMAT.Pages
{
    /// <summary>
    /// Interaction logic for Work.xaml
    /// </summary>
    /// 
    public partial class Work : UserControl
    {

        string emailprovider, statuscode, server, username, password, endnumber;
        DateTime date;
        int end, port, totalFound;

        CancellationTokenSource cts;
        string numbers = "";
        int filenumber = 0;
        bool usessl,sortRecent;
        string path = "";


        public Work()
        {
            InitializeComponent();

            providercombobox.SelectedItem = providercombobox.Items.GetItemAt(0);
            DatePicker.SelectedDate = DateTime.Now;

        }

        private async void beginbutton_Click(object sender, RoutedEventArgs e)
        {
           

                //initialize parameters
                log.Visibility = Visibility.Visible;
                port = int.Parse(portTextBox.Text);

                server = providercombobox.Text;

                switch (server)
                {
                    case "Gmail":
                        emailprovider = "imap.gmail.com";
                        break;
                    case "Yahoo":
                        emailprovider = "imap.mail.yahoo.com";
                        break;
                    default:
                        emailprovider = providercombobox.Text;
                        break;
                }


                if (sslComboBox.SelectionBoxItem.ToString() == "True") {
                    usessl = true;
                } else { usessl = false; }

                if (SortRecent.IsChecked == true)
                {
                    sortRecent = true;
                } else { sortRecent = false; }

                result.Text = "";
                result.Visibility = Visibility.Hidden;
                cancelButton.Visibility = Visibility.Visible;
                username = emailtextbox.Text;
                password = passwordtextbox.Password;
                log.Text = "";
                log.Visibility = Visibility.Visible;
                date = (DateTime)DatePicker.SelectedDate;
                endnumber = mailNumberTextbox.Text;
                end = int.Parse(endnumber);
                statuscode = "1";
                filenumber = 0;
                ProgressBar.Visibility = Visibility.Visible;
                path = Directory.GetCurrentDirectory() + "/attachments/";
                cts = new CancellationTokenSource();
                cts.Token.Register(delegate { canceled(); });

                ModernDialog.ShowMessage("Process Started", "Status", MessageBoxButton.OK);

                log.Text = DateTime.Now.ToString() + ": Connecting to email provider \n";
          

            try
            {
                //check if saving directory exists

                if (Directory.Exists(Directory.GetCurrentDirectory() + "/attachments"))
                {
                    string[] files = Directory.GetFiles(Directory.GetCurrentDirectory() + "/attachments");
                    foreach (var file in files)
                    {
                        File.Delete(file);
                    }
                }

                else
                {
                    Directory.CreateDirectory(Directory.GetCurrentDirectory() + "/attachments");
                }

            }
            catch
            {

                log.AppendText(DateTime.Now.ToString() + ": Sorry, couldn't create or empty attachment directory. \n");
log.ScrollToEnd();
            }


            try
            {
                await DoWork(cts.Token);

            }


            catch (OperationCanceledException)
            {
                Dispatcher.Invoke(delegate {

                    cancelButton.Visibility = Visibility.Hidden;
                    log.Text = DateTime.Now.ToString() + ": Operation Canceled \n";
                    log.Visibility = Visibility.Visible;
                    result.Visibility = Visibility.Hidden;
                    date = (DateTime)DatePicker.SelectedDate;
                    statuscode = "0";
                    ProgressBar.Visibility = Visibility.Hidden;

                });

            }

            catch
            {
                ModernDialog.ShowMessage("Please check the details you entered", "Error", MessageBoxButton.OK);
                return;
            }




        }

        private void OpenFromFileButton_Click(object sender, RoutedEventArgs e)
        {
            try {

                cts = new CancellationTokenSource();
                cts.Token.Register(delegate { canceled(); });

                System.Windows.Forms.FolderBrowserDialog Browser = new System.Windows.Forms.FolderBrowserDialog();
                System.Windows.Forms.DialogResult DialogResult = Browser.ShowDialog();

                if (DialogResult == System.Windows.Forms.DialogResult.OK)
                {
                   

                    path = Browser.SelectedPath;
                    var ScanResult = SyncedHelpers.ScanNumbers(this, path, cts.Token);
                    numbers = ScanResult.Item1;
                    filenumber = ScanResult.Item2;
                    totalFound = ScanResult.Item3;
                    if(filenumber < 1)
                    {
                        Dispatcher.Invoke(delegate {
                            ModernDialog.ShowMessage("No documents in folder.", "Status", MessageBoxButton.OK);
                        });
                        return;  
                    } 

                    Dispatcher.Invoke(delegate
                    {

                        log.AppendText(DateTime.Now.ToString() + ": Process completed. \n");
                        log.ScrollToEnd();
                        log.Visibility = Visibility.Visible;
                        ProgressBar.Visibility = Visibility.Hidden;
                        result.Text = numbers;
                        result.Visibility = Visibility.Visible;
                        cancelButton.Visibility = Visibility.Hidden;
                        ModernDialog.ShowMessage("Process Completed \n " + filenumber + " files processed in total \n" + totalFound + " numbers found", "Status", MessageBoxButton.OK);

                    });
                }
                else
                {
                    return;
                }

            } catch
            {
                Dispatcher.Invoke(delegate {
                    log.AppendText("Sorry an error occured.");
    log.ScrollToEnd();
                });
            }

        }

        private async Task DoWork(CancellationToken ct) {


            await Task.Run(async () =>
            {

                //get attachment message content, ids and status code

                var attachments = SyncedHelpers.ScanAttachments(date, emailprovider, username, password, port, usessl,sortRecent, end, ct, this);

                statuscode = attachments.Item2;
                var messages = attachments.Item1;

                if (statuscode != "0") //dont continue if theres an error
                {
                    //download attachments
                    try
                    {
                        statuscode = await SyncedHelpers.DownloadAttachments(messages, path, this, ct);
                    }
                    catch
                    {
                        Dispatcher.Invoke(delegate
                        {
                            log.AppendText(DateTime.Now.ToString() + ": Couldn't download attachments. \n");
            log.ScrollToEnd();

                        });

                    }
                }

                lock (this)
                {
                    if (statuscode != "0")
                    {

                        var ScanResult = SyncedHelpers.ScanNumbers(this, path, ct);
                        numbers = ScanResult.Item1;
                        filenumber = ScanResult.Item2;

                        try
                        {


                            string destination = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "/WOK-SMAT scanned attachments";
                            try
                            {                    //copy attachments to desktop
                                if (Directory.Exists(destination))
                                {
                                    string[] desktopfiles = Directory.GetFiles(destination);
                                    foreach (var file in desktopfiles)
                                    {
                                        File.Delete(file);
                                    }
                                    Directory.Delete(destination);

                                    Directory.Move(Directory.GetCurrentDirectory() + "/attachments", destination);

                                }
                                else
                                {
                                    Directory.Move(Directory.GetCurrentDirectory() + "/attachments", destination);

                                }
                            }
                            catch
                            {


                                Dispatcher.Invoke(delegate
                                {
                                    log.AppendText(DateTime.Now.ToString() + ": Couldnt copy files to desktop \n");
                    log.ScrollToEnd();

                                });
                            }


                            //Notify user
                            Dispatcher.Invoke(delegate
                            {

                                log.AppendText(DateTime.Now.ToString() + ": Process completed. \n");
                log.ScrollToEnd();
                                ProgressBar.Visibility = Visibility.Hidden;
                                result.Text = numbers;
                                result.Visibility = Visibility.Visible;
                                cancelButton.Visibility = Visibility.Hidden;
                                ModernDialog.ShowMessage("Process Completed \n " + filenumber + " files processed in total", "Status", MessageBoxButton.OK);

                            });


                        } //end try
                        catch (IOException ex)
                        {
                            lock (this)
                            {
                                Dispatcher.Invoke(delegate
                                {
                                    log.AppendText(DateTime.Now.ToString() + ": " + ex.Message + " \n");
                    log.ScrollToEnd();
                                    ProgressBar.Visibility = Visibility.Hidden;
                                    cancelButton.Visibility = Visibility.Hidden;

                                });
                            } //end lock
                        }
                        catch (Exception ex)
                        {
                            lock (this)
                            {
                                Dispatcher.Invoke(delegate
                                {
                                    log.AppendText(DateTime.Now.ToString() + ": an error occured  \n");
                                    log.AppendText(DateTime.Now.ToString() + ex.Message + " \n");
                    log.ScrollToEnd();


                                    result.Text = numbers;

                                    string path = Directory.GetCurrentDirectory() + @"/log" + DateTime.Now.ToString() + ".txt";

                                    File.WriteAllText(path, log.Text);
                                    result.Visibility = Visibility.Visible;
                                    ModernDialog.ShowMessage("Process Completed \n " + filenumber + " files processed in total", "Status", MessageBoxButton.OK);

                                    ProgressBar.Visibility = Visibility.Hidden;
                                    cancelButton.Visibility = Visibility.Hidden;

                                });
                            }//end lock

                        }

                    }// end if statement status code check

                }
            }, ct);// task.run

        }// end do work




        private void providercombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            providercombobox.IsEditable = true;

        }


        private void cancelButton_Click(object sender, RoutedEventArgs e) {

            cts.Cancel(true);

            Dispatcher.Invoke(delegate
            {
                log.Text += cts.Token.IsCancellationRequested.ToString();
            });


        }
        public void canceled() {
            Dispatcher.Invoke(delegate
            {

                cancelButton.Visibility = Visibility.Hidden;
                log.Text = DateTime.Now.ToString() + ": Operation Canceled";
                log.Visibility = Visibility.Visible;
                result.Visibility = Visibility.Hidden;
                date = (DateTime)DatePicker.SelectedDate;
                statuscode = "0";
                ProgressBar.Visibility = Visibility.Hidden;

            });

        }


        public async void CopyStream(Stream stream, string destPath)
        {

            using (var fileStream = new FileStream(destPath, FileMode.Create, FileAccess.Write))
            {
                await stream.CopyToAsync(fileStream);
            }

        }


    }
}

