﻿using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace WOK_SMAT.Pages
{
    /// <summary>
    /// Interaction logic for Home.xaml
    /// </summary>
    public partial class Home : UserControl
    {
        public Home()
        {
            InitializeComponent();
            
        }
        
        private void login_Click(object sender, RoutedEventArgs e)
        {
           
         

            try {
                var response = Api.Login(username.Text.Trim(' '), password.Password);

            if (response == "Login Successful")
            {
                BBCodeBlock bs = new BBCodeBlock();
                try
                {
                    
                    bs.LinkNavigator.Navigate(new Uri("/Pages/Work.xaml", UriKind.Relative), this);
                }
                catch (Exception error)
                {
                    ModernDialog.ShowMessage(error.Message, FirstFloor.ModernUI.Resources.NavigationFailed, MessageBoxButton.OK);
                }

            }
            ModernDialog.ShowMessage(response, "Status",MessageBoxButton.OK);

            }
            catch {
                ModernDialog.ShowMessage("Couldn't connect to the internet", "Status", MessageBoxButton.OK);
            }
        }
       
        public void Sync()
        {
            base.OnApplyTemplate();
            this.Dispatcher.Invoke(delegate {

                AppearanceViewModel j = new AppearanceViewModel();
                SolidColorBrush bru = new SolidColorBrush(j.SelectedAccentColor);
                passlabel.Foreground = bru;
                userlabel.Foreground = bru;

            });
        }

        private void clear_Click(object sender, RoutedEventArgs e)
        {
            username.Text = "";
            password.Password = "";
        }

        private void password_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                login_Click(sender, e);
            }
        }
    }
}
