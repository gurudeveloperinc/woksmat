﻿using System;
using System.Text;
using System.Net;
using System.Collections.Specialized;

namespace WOK_SMAT
{
    class Api
    {
		public static string Get(string url)
		{
			//get data from server
			WebClient client = new WebClient();
			Uri uri = new Uri(url);
			var reply = client.DownloadData(uri);
			var data = Encoding.UTF8.GetString(reply);

			return data;
		}

		public static byte[] Download(string url)
		{
			//download data from server
			WebClient client = new WebClient();
			Uri uri = new Uri("http://10.0.3.2/regent/getDept.php");
			var reply = client.DownloadData(uri);

			return reply;
		}

        public static string Login(string Username, string Password)
        {
            WebClient client = new WebClient();
            Uri uri = new Uri("http://woksmat.com.ng/loginpost.php");
            NameValueCollection parameter = new NameValueCollection();
            parameter.Add("u", Username);
            parameter.Add("p", Password);
            var reply = client.UploadValues(uri, parameter);
            var data = Encoding.UTF8.GetString(reply);

            return data;
        }


    }
}
