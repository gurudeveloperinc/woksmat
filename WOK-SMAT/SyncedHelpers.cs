﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using iTextSharp.text.pdf;

using System.IO;
using S22.Imap;
using System.Net.Mail;

using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using iTextSharp.text.pdf.parser;



using System.Text.RegularExpressions;
using System.Threading;
using IFilterTextReader;

using System.Runtime.Remoting.Contexts;
using WOK_SMAT.Pages;

namespace WOK_SMAT
{
    [Synchronization]
    class SyncedHelpers: ContextBoundObject
    {
        public static string GetTextFromAllPages(String pdfPath)
        {
            PdfReader reader = new PdfReader(pdfPath);

            StringWriter output = new StringWriter();

            for (int i = 1; i <= reader.NumberOfPages; i++)
                output.WriteLine(PdfTextExtractor.GetTextFromPage(reader, i, new SimpleTextExtractionStrategy()));
            reader.Close();

            return output.ToString();
        }

        public static Tuple<IEnumerable<MailMessage>, string> ScanAttachments(DateTime date, string EmailProvider, string Username, string Password, int Port, bool UseSsl, bool SortRecent, int End, CancellationToken ct, Work window)
        {
            ImapClient client;
            int current = 0;
            string statuscode = "1";
            IEnumerable<uint> uidSpecified, uids;
            try
            {
                client = new ImapClient(EmailProvider, Port, UseSsl, null);
                client.Login(Username, Password, AuthMethod.Auto);

                window.Dispatcher.Invoke(delegate {
                    window.log.AppendText(DateTime.Now.ToString() + ": Connected to email provider. Scanning emails \n");
                   window.log.ScrollToEnd();
                });
            }
            catch (Exception)
            {
                window.Dispatcher.Invoke(delegate
                {
                    window.log.AppendText(DateTime.Now.ToString() + ": Couldn't connect to email provider. \n");
                   window.log.ScrollToEnd();
                    statuscode = "0";
                    window.ProgressBar.Visibility = Visibility.Hidden;
                    window.cancelButton.Visibility = Visibility.Hidden;


                });
                throw new Exception();

            }

            if (SortRecent == true)
            {
                uids = client.Search(SearchCondition.SentSince(date));
                uidSpecified = uids.OrderByDescending(f => f).Take(End);
            } else
            {
                uids = client.Search(SearchCondition.SentSince(date));
                uidSpecified = uids.Take(End);
            }

            IEnumerable<MailMessage> messages = client.GetMessages(uidSpecified, (Bodypart part) =>
            {
                ct.ThrowIfCancellationRequested();
                ///make sure the attachment is not a video, audio or app
                if (part.Disposition.Type == ContentDispositionType.Attachment)
                {
                    //Dispatcher.Invoke(delegate {
                    //   log.AppendText( part.Subtype.ToLower() + "\n");
                    //});
                    current++;
                    window.Dispatcher.Invoke(delegate
                    {
                        window.log.AppendText(DateTime.Now.ToString() + ": Scanned attachment " + current.ToString() + "\n ");
                       window.log.ScrollToEnd();

                    });
                    if (part.Subtype.ToLower() == "pdf" || part.Subtype.ToLower() == "msword" || part.Subtype.ToLower() == "vnd.openxmlformats-officedocument.wordprocessingml.document")
                    {
                        return true;
                    }
                    else { return false; }



                }
                else
                {
                    return false;
                }

            }
            );// end delegate

            int count = 0;
            foreach (var item in uidSpecified)
            {
                count++;
            }

            string status = DateTime.Now.ToString() + ": Scanned " + count.ToString() + " emails \n";
            window.Dispatcher.Invoke(delegate
            {
                window.log.AppendText(status);
               window.log.ScrollToEnd();


            });

            return new Tuple<IEnumerable<MailMessage>, string>(messages, statuscode);
        }

        public static async Task<string> DownloadAttachments(IEnumerable<MailMessage> messages, string destPath, Work window, CancellationToken ct)
        {
            int current = 0;
            string statuscode = "0";

            foreach (var message in messages)
            {
                ct.ThrowIfCancellationRequested();
                AttachmentCollection j = message.Attachments;
                foreach (var item in j)
                {


                    using (var fileStream = new FileStream(destPath + item.Name, FileMode.Create, FileAccess.Write))
                    {
                        await item.ContentStream.CopyToAsync(fileStream);
                    }


                    item.Dispose();

                }

                current++;
            } // end foreach for attachment download

            if (current > 0)
            {
                window.Dispatcher.Invoke(delegate
                {
                    window.log.AppendText(DateTime.Now.ToString() + ": Attachments Downloaded \n");
                   window.log.ScrollToEnd();

                });
                statuscode = "1";
            }
            else
            {
                statuscode = "0";
            }

            return statuscode;
        }

        public static Tuple<string,int,int> ScanNumbers(Work window, string path, CancellationToken ct)
        {
            int NumOfFiles = 0;
            int currentDocxFileCount = 0;
            int currentDocFileCount = 0;
            int currentPdfFileCount = 0;
            string fulltext = "", numbers = "", currentFile = "";


            window.Dispatcher.Invoke(delegate
            {
                window.log.AppendText(DateTime.Now.ToString() + ": Opening files \n");
               window.log.ScrollToEnd();

            });

            string[] files = Directory.GetFiles(path);

            string[] docxFiles = Directory.GetFiles(path, "*.docx");

            string[] docFiles = Directory
            .GetFiles(path, "*.*")
            .Where(file => file.ToLower().EndsWith("doc"))
            .ToArray();

            string[] pdfFiles = Directory.GetFiles(path, "*.pdf");

            NumOfFiles = files.Length;


            window.Dispatcher.Invoke(delegate
            {
                window.log.AppendText(DateTime.Now.ToString() + ": There are " + files.Length + " files. \n");
                window.log.AppendText(DateTime.Now.ToString() + ": " + docxFiles.Length + " docx files. \n");
                window.log.AppendText(DateTime.Now.ToString() + ": " + pdfFiles.Length + " pdf files. \n");
                window.log.AppendText(DateTime.Now.ToString() + ": " + docFiles.Length + " doc files. \n");
                window.log.AppendText(DateTime.Now.ToString() + ": Scanning docx files. \n");
               window.log.ScrollToEnd();

            });



            while (currentDocxFileCount < docxFiles.Length)
            {
                ct.ThrowIfCancellationRequested();
                //open docx attachments and get numbers

                try
                {

                    for (int i = currentDocxFileCount; i <= docxFiles.Length; i++)
                    {

                        currentFile = docxFiles[i];
                        WordprocessingDocument document = WordprocessingDocument.Open(docxFiles[i] , false);

                        Body body = document.MainDocumentPart.Document.Body;

                        string Text = body.InnerText;
                        fulltext += Text;
                        Regex regex = new Regex(regexmatches.MatchNumber(), RegexOptions.IgnoreCase | RegexOptions.Singleline);


                        MatchCollection match = regex.Matches(Text);


                        if (match.Count > 0)
                        {

                            foreach (Match number in match)
                            {
                                numbers += number.Groups[0].Value + ",";
                            }
                        }



                        document.Close();
                        document.Dispose();

                        currentDocxFileCount++;

                    }// foreach for attachments     

                } // end try

                catch (Exception e)
                {
                    string filename = currentFile.Split('/').Last<string>();

                    window.Dispatcher.Invoke(delegate
                    {

                        window.log.AppendText(DateTime.Now.ToString() + ": Error occurred at docx file number " + currentDocxFileCount + "\n Named: " + filename + "\n");
                        window.log.AppendText(DateTime.Now.ToString() + ": " + e.Message + "\n");
                        window.log.ScrollToEnd();

                        currentDocxFileCount++;
                    });

                } // end catch

            } //end while

            

            window.Dispatcher.Invoke(delegate
            {
                window.log.AppendText(DateTime.Now.ToString() + ": Scanning pdf files. \n");
               window.log.ScrollToEnd();
                window.log.ScrollToEnd();

            });


            while (currentPdfFileCount < pdfFiles.Length)
            {
                ct.ThrowIfCancellationRequested();

                try
                {
                    if (pdfFiles != null)
                    {
                        //get numbers from pdf files
                        for(int i= currentPdfFileCount; i <= pdfFiles.Length; i++)
                        {
                            currentFile = pdfFiles[i];

                            string Text = GetTextFromAllPages(pdfFiles[i]);
                            fulltext += Text;

                            Regex regex = new Regex(regexmatches.MatchNumber(), RegexOptions.IgnoreCase | RegexOptions.Singleline);


                            MatchCollection match = regex.Matches(Text);

                            if (match.Count > 0)
                            {

                                foreach (Match number in match)
                                {
                                    numbers += number.Groups[0].Value + ",";
                                }
                            }



                            currentPdfFileCount++;

                        }// foreach for attachments
                    }

                } // end try

                catch (Exception)
                {
                    string filename = currentFile.Split('/').Last<string>();

                    window.Dispatcher.Invoke(delegate
                    {

                        window.log.AppendText(DateTime.Now.ToString() + ": Error occurred at pdf file number " + currentPdfFileCount + "\n Named: " + filename + "\n");
                       window.log.ScrollToEnd();

                        currentPdfFileCount++;
                    });

                } // end catch

            } // end while for pdf

            

            window.Dispatcher.Invoke(delegate
            {
                window.log.AppendText(DateTime.Now.ToString() + ": Scanning doc files. \n");
               window.log.ScrollToEnd();

            });


            while (currentDocFileCount < docFiles.Length)
            {

                ct.ThrowIfCancellationRequested();

                try
                {
                    //get numbers from doc files
                    for (int i = currentDocFileCount; i <= docFiles.Length; i++)
                    {
                        currentFile = docFiles[i];
                        TextReader reader = new FilterReader(docFiles[i]);


                        string Text = reader.ReadToEnd();
                        fulltext += Text;

                        Regex regex = new Regex(regexmatches.MatchNumber(), RegexOptions.IgnoreCase | RegexOptions.Singleline);


                        MatchCollection match = regex.Matches(Text);


                        if (match.Count > 0)
                        {

                            foreach (Match number in match)
                            {
                                numbers += number.Groups[0].Value + ",";
                            }
                        }


                        reader.Dispose();

                        currentDocFileCount++;

                    }// foreach for attachments

                } // end try

                catch (Exception)
                {
                    string filename = currentFile.Split('/').Last<string>();

                    window.Dispatcher.Invoke(delegate
                    {

                        window.log.AppendText(DateTime.Now.ToString() + ": Error occurred at doc file number " + currentDocFileCount + "\n Named: " + filename + "\n");
                        window.log.ScrollToEnd();

                        currentDocFileCount++;
                    });

                } // end catch

            }// end while for doc

            File.WriteAllText("alldata.txt", fulltext);
            
            // remove unnecessary characters
            string cleanNumbers = "";

            char[] thingsToRemove = { '.', '\n', '(', ')', '[', ']',':','-', '\\','/',';','|' };

            int index = 0;
            foreach (var thing in thingsToRemove)
            {
                if(index < 1)
                cleanNumbers = numbers.Replace(thing, ' ');
                else
                cleanNumbers = cleanNumbers.Replace(thing, ' ');
                index++;
            }

            // remove empty spaces
            List<string> check = cleanNumbers.Split(',').ToList();


            for (int i = 0; i < check.Count(); i++)
            {
                if (check[i] == "") check.RemoveAt(i);
            }

            // remove duplicates
            List<string> notRepeated = check.Distinct().ToList();
           
             
            cleanNumbers = "";

            
            for (int i = 0; i < notRepeated.Count(); i++)
            {
                cleanNumbers += notRepeated[i] + ",";
            }

           
            


            return new Tuple<string, int,int>( cleanNumbers, NumOfFiles,notRepeated.Count());

        }

    }

}
